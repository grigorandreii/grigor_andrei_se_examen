public class Diagram {

}

class A {

}

class B extends A{
    private long t;

    public void b(){}
}
// The class B inherits the properties and methods in A

class C {

}
// The class C is in a dependency relation with B
// meaning that it will use objects of B and their methods

class D {
    public void met1(int i){}
}

// Between the classes B and D is an association relationship
// meaning there will be a relation between the objects of the two classes

class E{
    public void met1(){}
}

// Class E is in a composition relationship with D
// This means that there can be no instances of E without at least an instance of D

class G{
    public double met3(){
        double i = 3.14;
        return i;
    }
}

// Class G is an aggregation relationship with D
// This means that there is a relationship between the instances of the classes
// However unlike in composition, instances of G can exist without instances of G

class F {
    public void n(String s){

    }
}

// The class F is also in an aggregation relationship with D
// there is a correlation between the instances of the two classes
// but they can exist independently of each other