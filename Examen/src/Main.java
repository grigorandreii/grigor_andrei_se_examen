import java.text.SimpleDateFormat;
import java.util.Date;

class MyThread extends Thread {

    MyThread(String s){
        setName(s);
    }

    @Override
    public void run(){
        for(int i =0; i<=7;i++){
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
            Date date = new Date(System.currentTimeMillis());
            System.out.println(getName() +" - "+ formatter.format(date));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


public class Main {
    public static void main(String[] args) {
        MyThread t1 = new MyThread("HI-Thread1");
        MyThread t2 = new MyThread("HI-Thread2");
        MyThread t3 = new MyThread("HI-Thread3");
        MyThread t4 = new MyThread("HI-Thread4");
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}


